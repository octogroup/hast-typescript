import { Either } from '@octogroup/fairy-land';
import { Rose, Nary } from '@octogroup/fairy-land/lib/concurrent';
import { HastElement } from './makeHast';
export declare type DomTree<Props> = Rose<Props, Either<Text, HastElement>>;
export declare type TextNode = Nary<undefined, {
    tagName: 'h1';
    value: string;
}>;
