"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fairy_land_1 = require("@octogroup/fairy-land");
exports.htmlDoctype = {
    type: 'doctype',
    name: 'html'
};
exports.makeHTML = (doc) => (language) => {
    if (doc.head && doc.body) {
        if (language) {
            return [
                exports.htmlDoctype,
                {
                    type: 'element',
                    tagName: 'html',
                    properties: {
                        lang: language
                    },
                    children: [
                        doc.head,
                        doc.body,
                    ]
                }
            ];
        }
        else {
            return [
                exports.htmlDoctype,
                {
                    type: 'element',
                    tagName: 'html',
                    children: [
                        doc.head,
                        doc.body,
                    ]
                }
            ];
        }
    }
    else {
        if (doc.head) {
        }
        else {
            throw new Error('Missing Hast Head.');
        }
        if (doc.body) {
        }
        else {
            throw new Error('Missing Hast Body.');
        }
        throw new Error('Missing required Hast element Head and/or Body.');
    }
};
exports.makeHastRoot = (children) => (language) => ({
    type: 'root',
    children: exports.makeHTML(children)(language)
});
exports.make = {
    text: (s) => ({ type: 'text', value: s }),
    element: (element) => {
        if (element.properties) {
            return {
                type: 'element',
                tagName: element.tag,
                properties: element.properties,
                children: fairy_land_1.Array.compact(element.children)
            };
        }
        else {
            return {
                type: 'element',
                tagName: element.tag,
                children: fairy_land_1.Array.compact(element.children)
            };
        }
    },
};
exports.appendChildNode = (parent) => (child) => (Object.assign(Object.assign({}, parent), { children: [
        ...parent.children,
        child
    ] }));
//# sourceMappingURL=makeHast.js.map