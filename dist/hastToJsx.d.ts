/// <reference types="react" />
import { HastElement, HastText } from './makeHast';
declare type HastNode = HastElement | HastText;
export declare const hastToJsx: (hastNode: HastNode) => string | JSX.Element;
export {};
