export interface HastPosition {
    start: HastPoint;
    end: HastPoint;
    indent: number[];
}
export interface HastPoint {
    line: number;
    column: number;
    offset: number;
}
export interface HastData {
    [key: string]: any;
}
export interface HastNode {
    type: string;
    data?: HastData;
    position?: HastPosition;
}
export interface HastLiteral extends HastNode {
    value: any;
}
export interface HastText extends HastLiteral {
    type: "text";
    value: string;
}
export interface HastDoctype extends HastNode {
    type: "doctype";
    name: string;
    public?: string;
    system?: string;
}
export interface HastParent extends HastNode {
    children: HastNode[];
}
export interface HastRoot extends HastParent {
    type: "root";
}
export declare type HastElementType = 'p' | 'h1' | 'h2' | 'h3' | 'h4' | 'h5' | 'h6' | 'link' | 'label' | 'meta' | 'title' | 'a' | 'div' | 'span' | 'script' | 'img' | 'ul' | 'ol' | 'li' | 'blockquote' | 'br' | 'form' | 'button' | 'input' | 'textarea' | 'option' | 'select' | 'embed' | 'iframe' | 'head' | 'body' | 'html';
export interface HastProperties {
    async?: 'async';
    defer?: 'defer';
    allowFullScreen?: boolean;
    className?: string;
    content?: string;
    cursor?: string;
    crossorigin?: boolean;
    frameBorder?: string;
    form?: string;
    height?: string;
    hidden?: boolean;
    href?: string;
    hrefLang?: string;
    htmlFor?: string;
    httpEquiv?: string;
    id?: string;
    label?: string;
    lang?: string;
    link?: string;
    max?: string;
    media?: string;
    min?: string;
    multiple?: boolean;
    name?: string;
    onclick?: (ev: MouseEvent) => void;
    onchange?: (ev: Event) => void;
    ondrag?: (ev: DragEvent) => void;
    ondragend?: (ev: DragEvent) => void;
    ondragenter?: (ev: DragEvent) => void;
    ondragleave?: (ev: DragEvent) => void;
    ondragover?: (ev: DragEvent) => void;
    ondragstart?: (ev: DragEvent) => void;
    ondrop?: (ev: DragEvent) => void;
    oninput?: (ev: Event) => void;
    oninvalid?: (ev: Event) => void;
    onoffline?: (ev: Event) => void;
    ononline?: (ev: Event) => void;
    onsubmit?: (ev: Event) => void;
    onload?: (ev: Event) => void;
    selected?: boolean;
    checked?: boolean;
    origin?: string;
    placeholder?: string;
    rel?: string;
    required?: boolean;
    scale?: string;
    sizes?: string;
    src?: string;
    style?: string;
    target?: string;
    title?: string;
    type?: string;
    value?: string;
    width?: string;
    wrap?: string;
    compositionstart?: (ev: Event) => void;
    compositionupdate?: (ev: Event) => void;
    compositionend?: (ev: Event) => void;
    reset?: (ev: Event) => void;
    submit?: (ev: Event) => void;
}
export interface HastElement extends HastParent {
    type: 'element';
    tagName: HastElementType;
    children: Array<HastNode>;
    properties?: HastProperties;
}
export interface HastLink extends HastElement {
    tagName: 'a';
}
export interface HastHead extends HastElement {
    tagName: 'head';
}
export interface HastBody extends HastElement {
    tagName: 'body';
}
export interface HastHTMLPage {
    docType: HastDoctype;
    head: HastHead;
    body: HastBody;
}
export declare const htmlDoctype: HastDoctype;
export declare const makeHTML: (doc: {
    head: HastHead;
    body: HastBody;
}) => (language?: string | undefined) => (HastNode | HastElement)[];
export declare const makeHastRoot: (children: {
    head: HastHead;
    body: HastBody;
}) => (language: string) => HastRoot;
export interface ElementBuilder {
    tag: HastElementType;
    children: Array<HastNode>;
    properties?: HastProperties;
}
export interface Helpers {
    text: (s: string) => HastText;
    element: (element: ElementBuilder) => HastElement;
}
export declare const make: Helpers;
export declare const appendChildNode: (parent: HastElement) => (child: HastNode) => HastElement;
