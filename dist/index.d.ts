import { HastBody, HastData, HastDoctype, HastElement, ElementBuilder, htmlDoctype, HastHTMLPage, HastHead, Helpers, HastLink, HastLiteral, make, makeHastRoot, HastNode, HastParent, HastPoint, HastPosition, HastProperties, HastRoot, HastText, appendChildNode, makeHTML } from './makeHast';
export { HastBody, HastData, HastDoctype, HastElement, ElementBuilder, htmlDoctype, HastHTMLPage, HastHead, Helpers, HastLink, HastLiteral, make, makeHastRoot, HastNode, HastParent, HastPoint, HastPosition, HastProperties, HastRoot, HastText, appendChildNode, makeHTML };
import { DomTree, TextNode } from './components';
export { DomTree, TextNode };
import { hastToJsx } from './hastToJsx';
export { hastToJsx };
export declare const sanitize: any;
