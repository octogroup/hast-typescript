"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const makeHast_1 = require("./makeHast");
exports.htmlDoctype = makeHast_1.htmlDoctype;
exports.make = makeHast_1.make;
exports.makeHastRoot = makeHast_1.makeHastRoot;
exports.appendChildNode = makeHast_1.appendChildNode;
exports.makeHTML = makeHast_1.makeHTML;
const hastToJsx_1 = require("./hastToJsx");
exports.hastToJsx = hastToJsx_1.hastToJsx;
exports.sanitize = require('hast-util-sanitize');
//# sourceMappingURL=index.js.map