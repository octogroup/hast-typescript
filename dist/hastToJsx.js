"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = require("react");
exports.hastToJsx = (hastNode) => {
    if (hastNode.type === 'element') {
        return react_1.createElement(hastNode.tagName, hastNode.properties, hastNode.children);
    }
    else {
        return hastNode.value;
    }
};
//# sourceMappingURL=hastToJsx.js.map