import { Array } from '@octogroup/fairy-land';

// unist type definitions copied from https://github.com/syntax-tree/unist-ts
// Location of a node in a source file.
export interface HastPosition {
   // Place of the first character of the parsed source region.
   start: HastPoint
   // Place of the first character after the parsed source region.
   end: HastPoint
   // Start column at each index (plus start line) in the source region,
   // for elements that span multiple lines.
   indent: number[]
}

// One place in a source file.
export interface HastPoint {
   // Line in a source file (1-indexed integer).
   line: number
   // Column in a source file (1-indexed integer).
   column: number
   // Character in a source file (0-indexed integer).
   offset: number
}

// Information associated by the ecosystem with the node.
// Space is guaranteed to never be specified by unist or specifications
// implementing unist.
export interface HastData {
   [key: string]: any
}

// Syntactic units in unist syntax trees are called nodes.
export interface HastNode {
   // The variant of a node.
   type: string
   // Information from the ecosystem.
   data?: HastData
   // Location of a node in a source document.
   // Must not be present if a node is generated.
   position?: HastPosition
}

// hast-specific definitions
// Nodes containing a value.
export interface HastLiteral extends HastNode {
   value: any
}

// Plain text (like the children of an H1)
export interface HastText extends HastLiteral {
   type: "text",
   value: string
}

export interface HastDoctype extends HastNode {
   type: "doctype"
   name: string
   public?: string
   system?: string
}

// Nodes containing other nodes.
export interface HastParent extends HastNode {
   // List representing the children of a node.
   children: HastNode[]
}

// The "root" node represents the entire document.
// All child nodes derive from the root.
export interface HastRoot extends HastParent {
   type: "root"
}
// Allowed HTML elements
export type HastElementType = 'p' | 'h1' | 'h2' | 'h3' | 'h4' | 'h5' | 'h6' | 'link' | 'label' |'meta' | 'title' | 'a' | 'div' | 'span' | 'script' | 'img' | 'ul' | 'ol' | 'li' | 'blockquote' | 'br' | 'form' | 'button' | 'input' | 'textarea' | 'option' | 'select' | 'embed' | 'iframe' | 'head' | 'body' | 'html';

// https://github.com/wooorm/property-information
// https://www.w3schools.com/jsref/dom_obj_event.asp
export interface HastProperties {
   async?: 'async'
   defer?: 'defer'
   allowFullScreen?: boolean
   className?: string
   content?: string
   cursor?: string
   crossorigin?: boolean
   frameBorder?: string
   form?: string
   height?: string
   hidden?: boolean
   href?: string
   hrefLang?: string
   htmlFor?: string
   httpEquiv?: string
   id?: string
   label?: string
   lang?: string
   link?: string
   max?: string
   media?: string
   min?: string
   multiple?: boolean
   name?: string
   onclick?: (ev: MouseEvent) => void
   onchange?: (ev: Event) => void
   ondrag?: (ev: DragEvent) => void
   ondragend?: (ev: DragEvent) => void
   ondragenter?: (ev: DragEvent) => void
   ondragleave?: (ev: DragEvent) => void
   ondragover?: (ev: DragEvent) => void
   ondragstart?: (ev: DragEvent) => void
   ondrop?: (ev: DragEvent) => void
   oninput?: (ev: Event) => void
   oninvalid?: (ev: Event) => void
   onoffline?: (ev: Event) => void
   ononline?: (ev: Event) => void
   onsubmit?: (ev: Event) => void
   onload?: (ev: Event) => void
   selected?: boolean
   checked?: boolean
   origin?: string
   placeholder?: string
   rel?: string
   required?: boolean
   scale?: string
   sizes?: string
   src?: string
   style?: string
   target?: string
   title?: string
   type?: string
   value?: string
   width?: string
   wrap?: string
   // Text Events
   compositionstart?: (ev: Event) => void
   compositionupdate?: (ev: Event) => void
   compositionend?: (ev: Event) => void
   // Form events
   reset?: (ev: Event) => void
   submit?: (ev: Event) => void
}

export interface HastElement extends HastParent {
   type: 'element'
   tagName: HastElementType
   children: Array<HastNode>
   properties?: HastProperties
}

export interface HastLink extends HastElement {
   tagName: 'a'
}

export interface HastHead extends HastElement {
   tagName: 'head'
}

export interface HastBody extends HastElement {
   tagName: 'body'
}

export interface HastHTMLPage {
   docType: HastDoctype
   head: HastHead
   body: HastBody
}

export const htmlDoctype: HastDoctype = {
   type: 'doctype',
   name: 'html'
}

export const makeHTML = (doc: { head: HastHead, body: HastBody }) => (language?: string): Array<HastNode|HastElement> => {
   // Add null checks to ensure we're making valid Hast
   if (doc.head && doc.body) {
      // Make the HTML
      if (language) {
         return [
            htmlDoctype,
            {
               type: 'element',
               tagName: 'html',
               properties: {
                  lang: language
               },
               children: [
                  doc.head,
                  doc.body,
               ]
            }
         ]
      }
      else {
         return [
            htmlDoctype,
            {
               type: 'element',
               tagName: 'html',
               children: [
                  doc.head,
                  doc.body,
               ]
            }
         ]
      }
   }
   else {
      if (doc.head) {
      }
      else {
         throw new Error('Missing Hast Head.');
      }
      if (doc.body) {
      }
      else {
         throw new Error('Missing Hast Body.');
      }
      throw new Error('Missing required Hast element Head and/or Body.')
   }
}

/**
 * @param language 2-character language code, e.g. 'en' or 'fr'
 */
export const makeHastRoot = (children: { head: HastHead, body: HastBody }) => (language: string): HastRoot => ({
   type: 'root',
   children: makeHTML(children)(language)
})

export interface ElementBuilder {
   tag: HastElementType
   children: Array<HastNode>
   properties?: HastProperties
}

export interface Helpers {
   text: (s: string) => HastText
   element: (element: ElementBuilder) => HastElement
}

export const make: Helpers = {
   text: (s: string) => ({ type: 'text', value: s }),
   element: (element: ElementBuilder) => {
      if (element.properties) {
         return {
            type: 'element',
            tagName: element.tag,
            properties: element.properties,
            // Remove undefined and null values
            children: Array.compact(element.children)
         }
      }
      else {
         return {
            type: 'element',
            tagName: element.tag,
            // Remove undefined and null values
            children: Array.compact(element.children)
         }
      }
   },
}

/**
 * Add an element to another element's children
 */
export const appendChildNode = (parent: HastElement) => (child: HastNode): HastElement => ({
   ...parent,
   children: [
      ...parent.children,
      child
   ]
})