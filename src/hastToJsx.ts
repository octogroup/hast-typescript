import { createElement} from 'react';
import { HastElement, HastText } from './makeHast';
type HastNode = HastElement | HastText;

export const hastToJsx = (hastNode: HastNode): JSX.Element | string => {
   if (hastNode.type === 'element') {
      return createElement(hastNode.tagName, hastNode.properties, hastNode.children)
   } else {
      return hastNode.value
   }
}
