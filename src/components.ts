import { Either } from '@octogroup/fairy-land';
import { Rose, Nary, } from '@octogroup/fairy-land/lib/concurrent'
import { HastElement } from './makeHast';
/**
 * Provide a standard interface for writing components, that we'll later turn into Hast.
 */

export type DomTree<Props> = Rose<Props, Either<Text, HastElement>>
export type TextNode = Nary<undefined, { tagName: 'h1', value: string }>