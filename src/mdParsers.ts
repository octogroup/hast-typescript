// import { readFileSync } from 'fs';

//
// mdast Nodes
// https://github.com/syntax-tree/mdast/blob/master/readme.md#nodes
//
// type MdastNodes = 'heading' | 'blockquote' | 'list' | 'listItem' | 'html' | 'code' | 'text' | 'emphasis' | 'strong' | 'paragraph' | 'delete' | 'inlineCode' | 'break' | 'link' | 'image';

// // We need virtual files to work with Markdown.
// const vfile = require('vfile');
// export const markdownToVFile = (markdown: any) => vfile({ contents: markdown });

// // Markdown implements the unist spec as 'mdast'
// // Setup Markdown --> mdast
// // https://github.com/unifiedjs/unified
// const unified = require('unified');
// // https://github.com/remarkjs/remark/tree/master/packages/remark-parse
// const parseMarkdown = require('remark-parse');
// // Setup mdast --> hast
// // https://github.com/syntax-tree/mdast-util-to-hast
// const mdastToHast = require('mdast-util-to-hast');
// // https://github.com/zestedesavoir/zmarkdown/tree/master/packages/remark-disable-tokenizers
// const remarkDisableTokenizers = require('remark-disable-tokenizers');

// The Markdown processor loves to insert newlines (\n) which we need to remove
// const removeNewlineElements = <Props>(mdast: Root_<Props>) => {
//    const _newlineToEmptyString = (child: Element_<Props> | Text_): any => {
//       if (child.type === 'text') {
//          if (child.value === '\n') {
//             return child.value = ''
//          }
//       }
//       else if (child.type === 'element') {
//          return child.children.map(_newlineToEmptyString()
//       }
//       else {
//          return
//       }
//    }
//    const _removeEmptyStrings = (child: Element_<Props> | Text_): any => {
//       if (child.type === 'text') {
//          if (child.value === '') {
//             const { type, ...noValue } = child;
//             return noValue
//          }
//       }
//       else if (child.type === 'element') {
//          return _removeEmptyStrings(child)
//       }
//       else {
//          return
//       }
//    }

//    // Set newlines to empty strings
//    const noNewLines = mdast.children.map((child: Element_<Props> | Text_) => {
//       if (child.type === 'text') {
//          if (child.value  === '\n') {
//             return child.value = ''
//          }
//       }
//       else if (child.type === 'element') {
//          return _newlineToEmptyString(child)
//       }
//       else {
//          return
//       }
//    })

   // Remove empties
   // mdast.children.map((child: Element_<Props> | Text_) => {
   //    if (child.type === 'text') {
   //       if (child.value === '') {
   //          const { type, ...noValue } = child;
   //          return noValue
   //       }
   //    }
   //    else if (child.type === 'element') {
   //       return _removeEmptyStrings(child)
   //    }
   //    else {
   //       return
   //    }
   // })

   // Return the modified root
   // const mdastWithNoNewLines = mdast.children = noNewLines
   // console.log(mdastWithNoNewLines)
   // return mdastWithNoNewLines
// } 

// const markdownToMdast = (mdVFile: any) => unified().use(parseMarkdown, {commonmark: true}).use(remarkDisableTokenizers, {
//    block: [
//       'footnote',
//       'table',
//    ]
// }).parse(mdVFile);

// // Put all the Markdown process together!
// export const mdToHast = (mdVFile: any) => mdastToHast(
//    removeNewlineElements(
//       markdownToMdast(mdVFile)
//    )
// );


// // Testing! 
// const mdVFile = markdownToVFile(readFileSync('./test/test.md'));
// console.log(mdToHast(mdVFile));