# Heading 1

This is a paragraph

```html
<div class="form wrapper max">
   <div class="miniDescription">Please be sure that 1) you have the right to share your work publicly, and 2) you have consent from all Contributors (i.e. coauthors) to share the work.
   Please convert any Word documents to PDFs before submitting to MarXiv.
   </div>
   <div class="collapseBar flex all-parent-row">
      <div class="title flex all-child-span1">Before you submit</div>
      <div class="icon center">
         <img src="images/icons/minus-square.svg" class="right minimize"/>
         <img src="images/icons/plus-square.svg" class="right maximize"/>
      </div>
   </div>
   <div class="content">
   </div>
</div>
```